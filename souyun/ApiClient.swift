//
//  ApiClient.swift
//  souyun
//
//  Created by SkyGrass on 16/5/26.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import Alamofire

class ApiClient: NSObject {
    
    static func search(_request:ShowApiRequest,
                       _seachword:String?,
                       _pageindex:Int?=1,
                       _success:(data:[ViewData])->Void,
                       _failure:(code:Int,errmsg:String)->Void){
        
        var returnviewdata = [ViewData]()
        _request.post(["q":_seachword!,"page":_pageindex!], callback:{
            data in
            if NSJSONSerialization.isValidJSONObject(data){
                let showapi_res_code = data.objectForKey("showapi_res_code")as? Int
                let showapi_res_error = data.objectForKey("showapi_res_error")as? String
                
                if showapi_res_code! == 0{
                    if let contentArray : NSArray = data.objectForKey("showapi_res_body")!.objectForKey("pagebean")!.objectForKey("contentlist") as? NSArray{
                        for item in contentArray{
                            let _viewdata = ViewData()
                            _viewdata.content = item.objectForKey("content")! as? String
                            _viewdata.url = item.objectForKey("url")! as? String
                            _viewdata.title = item.objectForKey("title")! as? String
                            
                            returnviewdata.append(_viewdata)
                        }
                        _success(data:returnviewdata)
                    }
                }else{
                    _failure(code:showapi_res_code! ,errmsg: showapi_res_error!)
                }
            }
            else{
                 _failure(code:-1 ,errmsg: "no json")
            }
        })
    }
}