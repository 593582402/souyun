//
//  ListView.swift
//  souyun
//
//  Created by SkyGrass on 16/5/29.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit
import SnapKit

class ListView: UIView,UITableViewDataSource {
    var list:UITableView!
    private var data:Viewdatas!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect,_data:Viewdatas) {
        super.init(frame: frame)
        
        
        data = Viewdatas()
        data = _data
        list = UITableView()
        list.dataSource = self
        
        addSubview(list)
        
        list.registerClass(MyCell.self, forCellReuseIdentifier: "mycell")
        
        list.snp.makeConstraints { (make) in
            make.left.equalTo(self.snp.left)
            make.right.equalTo(self.snp.right)
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("mycell", forIndexPath: indexPath) as! MyCell
        
        let index = indexPath.row
        
        cell.textLabel?.text = data[index].title!
        cell.detailTextLabel?.text = data[index].content!
        
        return cell
    }
}
