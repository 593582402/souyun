//
//  HUD.swift
//  souyun
//
//  Created by SkyGrass on 16/5/26.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import Foundation
import MBProgressHUD

class HUD {
    static func show(view:UIView,message:String) ->MBProgressHUD{
        let hud : MBProgressHUD = MBProgressHUD.showHUDAddedTo(view, animated: true)
        hud.mode = .Text
        hud.labelText = message
        hud.show(true)
        hud.hide(true,afterDelay: 1.5)
        return hud
    }
}
