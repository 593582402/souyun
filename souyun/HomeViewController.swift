//
//  HomeViewController.swift
//  souyun
//
//  Created by SkyGrass on 16/5/26.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit
import SnapKit

class HomeViewController: UIViewController {
    var homeView:HomeView!
    override func viewDidLoad() {
        super.viewDidLoad()
        homeView = HomeView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
    }
    
    override func viewDidAppear(animated: Bool) {
        super.view.addSubview(homeView)
        homeView.btnSearch.addTarget(self, action: #selector(tap(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func tap(sender:AnyObject){
        if !homeView.txtSearch.text!.isEmpty{
            homeView.textFieldShouldReturn(homeView.txtSearch)
            self.performSegueWithIdentifier("show", sender: homeView.txtSearch.text!)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "show"{
            let nvcontroller = segue.destinationViewController as! UINavigationController
            let controller = nvcontroller.topViewController as! ListViewController // ViewController 转 UINavigationCtorller
            controller.navigationItem.title = "正在查询 " + (sender as! String) + "..."
            controller.searchword = sender as? String
        }
    }
}
