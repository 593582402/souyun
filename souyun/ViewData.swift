//
//  ViewData.swift
//  souyun
//
//  Created by SkyGrass on 16/5/26.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import Foundation

class ViewData: NSObject {
    var content:String?
    var title:String?
    var url:String?
}
