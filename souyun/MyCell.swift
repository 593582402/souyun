//
//  MyCell.swift
//  souyun
//
//  Created by SkyGrass on 16/5/29.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit

class MyCell: UITableViewCell {
   required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .Subtitle, reuseIdentifier: reuseIdentifier)
    }
}
