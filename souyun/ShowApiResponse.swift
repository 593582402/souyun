//
//  ShowApiResponse.swift
//  souyun
//
//  Created by SkyGrass on 16/5/26.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import ObjectMapper

class ShowApiResponse: Mappable{
    var showapi_res_code:Int?
    var showapi_res_error:String?
    var showapi_res_body:ShowApiBody?
    required init? (_:Map){
        
    }
    func mapping(map:Map){
        showapi_res_code <- map["showapi_res_code"]
        showapi_res_error <- map["showapi_res_error"]
        showapi_res_body <- map["showapi_res_body"]
    }
}
class ShowApiBody:Mappable{
    var ret_code:Int?
    var pagebean:PageBean?
    
    required init?(_:Map){
        
    }
    func mapping(map:Map){
        ret_code <- map["ret_code"]
        pagebean <- map["pagebean"]
    }
}

class PageBean:Mappable{
    var allPages:Int?
    var currentPage:Int?
    var allNum:Int?
    var maxResult:Int?
    var contentlist:[ContentList]?
    
    required init?(_:Map){
        
    }
    func mapping(map:Map){
        allPages <- map["allPages"]
        currentPage <- map["currentPage"]
        allNum <- map["allNum"]
        maxResult <- map["maxResult"]
        contentlist <- map["contentlist"]
    }
}

class ContentList:Mappable{
    var url:String?
    var title:String?
    var content:String?
    required init?(_:Map){
        
    }
    func mapping(map:Map){
        url <- map["url"]
        title <- map["title"]
        content <- map["content"]
    }
}
