//
//  ApiServiceProtocol.swift
//  souyun
//
//  Created by SkyGrass on 16/5/29.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import Foundation

typealias request = ShowApiRequest // 这个是类型的别名
typealias Viewdatas = [ViewData]
typealias completionHandler = (Viewdatas?,Error?) ->Void

//本来是 用block的 ，看了大神的帖子，还是换成 Protocol 吧，
//我也不知道为什么，反正就是换了
protocol ApiServiceProtocol {
    func search(_request:request,_searchword:String,_pageindex:Int,_completionHandler:completionHandler)
    
}