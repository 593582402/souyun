//
//  HomeView.swift
//  souyun
//
//  Created by SkyGrass on 16/5/28.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit

class HomeView: UIView,UITextFieldDelegate {
    private  var logoView : UIImageView!
    var txtSearch: UITextField!
    var btnSearch:UIButton!
    private  var inputview:UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
    
        logoView = UIImageView()
        txtSearch = UITextField()
        btnSearch = UIButton()
        inputview = UIView()
        
        logoView.image = UIImage(named: "search")
        logoView.contentMode = UIViewContentMode.ScaleAspectFit
        
        
        txtSearch.borderStyle = UITextBorderStyle.Line
        txtSearch.adjustsFontSizeToFitWidth = true
        txtSearch.minimumFontSize = 14
        txtSearch.textAlignment = .Left
        txtSearch.contentVerticalAlignment = .Center
        txtSearch.clearButtonMode = .WhileEditing
        txtSearch.keyboardType = .Default
        txtSearch.returnKeyType = .Go
        txtSearch.placeholder = "输入要搜索的资源!"
        txtSearch.tag = 100
        txtSearch.delegate = self
        
        
        btnSearch.setTitle("搜云", forState: UIControlState.Normal)
        btnSearch.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        btnSearch.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        btnSearch.setTitleColor(UIColor.grayColor(), forState: UIControlState.Disabled)
        btnSearch.layer.cornerRadius = 1
        btnSearch.layer.borderColor = UIColor.blackColor().CGColor
        btnSearch.layer.borderWidth = 1
        btnSearch.backgroundColor = UIColor.darkGrayColor()
        
        addSubview(logoView)
        addSubview(inputview)
        inputview.addSubview(txtSearch)
        inputview.addSubview(btnSearch)
        
        logoView.snp.makeConstraints{ (make) in
            make.leading.trailing.equalTo(-10)
            make.top.equalTo(self.snp.top).offset(100)
            make.height.equalTo(self.snp.height).multipliedBy(0.15)
        }
        
        inputview.snp.makeConstraints { (make) in
            make.top.equalTo(logoView.snp.bottom).offset(20)
            make.left.equalTo(self.snp.left).offset(20)
            make.right.equalTo(self.snp.right).inset(20)
            make.height.equalTo(self.snp.height).multipliedBy(0.08)
        }
        
        txtSearch.snp.makeConstraints { (make) in
            make.left.equalTo(inputview.snp.left)
            make.top.equalTo(inputview.snp.top)
            make.bottom.equalTo(inputview.snp.bottom)
            make.width.equalTo(inputview.snp.width).multipliedBy(0.8)
        }
        
        btnSearch.snp.makeConstraints { (make) in
            make.right.equalTo(inputview.snp.right)
            make.top.equalTo(inputview.snp.top)
            make.bottom.equalTo(inputview.snp.bottom)
            make.width.equalTo(inputview.snp.width).multipliedBy(0.21)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
