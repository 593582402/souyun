//
//  WebViewController.swift
//  souyun
//
//  Created by SkyGrass on 16/5/27.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {
    var webView : WebView!
    var waitView:WaitView?
    var url :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.translucent = false;
        
        
        webView = WebView(frame: CGRect(x: 0,y: 0,width: self.view.frame.width,height: self.view.frame.height))
        webView.showWebView.delegate = self
        
        webView.showWebView.loadRequest(NSURLRequest(URL: NSURL(string: url!)!))
        super.view.addSubview(webView)
        
        waitView = WaitView(frame: CGRect(x: 0 , y: 0, width: self.view.frame.width, height: self.view.frame.height))
        waitView?.labMsg.text = "正在跳转网页..."
        
        super.view.addSubview(waitView!)
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        self.waitView?.hidden = true
    }
}
