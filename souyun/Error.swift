//
//  Error.swift
//  souyun
//
//  Created by SkyGrass on 16/5/29.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import Foundation

class Error: NSObject {
    
    var code:Int? = -1
    var errMsg : String?
    
    init(_code:Int?,_errMsg:String?) {
        code = _code
        errMsg = _errMsg
    }
}
