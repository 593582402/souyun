//
//  WaitView.swift
//  souyun
//
//  Created by SkyGrass on 16/5/29.
//  Copyright © 2016年 SkyGrass. All rights reserved.
//

import UIKit
import SnapKit

class WaitView: UIView {
    var labMsg = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
        labMsg.text = "正在加载数据..."
        labMsg.backgroundColor = UIColor.whiteColor()
        labMsg.textColor = UIColor.blackColor()
        
        addSubview(labMsg)
        
        labMsg.snp.makeConstraints { (make) in
            make.centerX.centerY.equalTo(self.snp.center)
        }
    }
    
}
